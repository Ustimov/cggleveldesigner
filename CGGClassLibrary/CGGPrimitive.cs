﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
//using System.Drawing;

namespace CGGClassLibrary
{
    public enum CGGFigureType
    {
        NONE = 0,
        HOUSE = 1,
        PIPE = 2,
    }

    //[Serializable]
    [DataContract]
    public abstract class CGGPrimitive
    {
        [DataMember]
        private int x;

        [DataMember]
        private int y;

        [DataMember]
        private double scaleX = 1;

        [DataMember]
        private double scaleY = 1;

        [DataMember]
        private CGGFigureType figureType = CGGFigureType.NONE;

        public CGGPrimitive(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public int X
        {
            get { return x; }
            set { x = value; }
        }

        public int Y
        {
            get { return y; }
            set { y = value; }
        }

        public double ScaleX
        {
            get { return scaleX; }
            set { scaleX = value; }
        }

        public double ScaleY
        {
            get { return scaleY; }
            set { scaleY = value; }
        }

        public CGGFigureType FigureType
        {
            get { return figureType; }
            set { figureType = value; }
        }

        public abstract List<CGGPoint> GetPoints(double k);
        public abstract bool Contains(CGGPoint p, double k);
    }
}
