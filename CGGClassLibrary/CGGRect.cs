﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
//using System.Drawing;

namespace CGGClassLibrary
{
    [DataContract]
    public class CGGRect : CGGPrimitive
    {
        [DataMember]
        private int width = 50;

        [DataMember]
        private int height = 50;

        public CGGRect(int x, int y) : base(x, y)
        {

        }

        public override List<CGGPoint> GetPoints(double k)
        {
            var X = (int)(this.X * k);
            var Y = (int)(this.Y * k);

            return new List<CGGPoint>
            {
                new CGGPoint(X, Y),
                new CGGPoint(X, (int)(Y + height * k * ScaleY)),
                new CGGPoint((int)(X + width * k * ScaleX), (int)(Y + height * k * ScaleY)),
                new CGGPoint((int)(X + width * k * ScaleX), Y)
            };
        }

        public override bool Contains(CGGPoint p, double k)
        {
            var X = (int)(this.X * k);
            var Y = (int)(this.Y * k);

            return p.X >= X && p.X <= X + width * k * ScaleX
                && p.Y >= Y && p.Y <= Y + height * k * ScaleY;
        }
    }
}
