﻿using System;

namespace CGGClassLibrary
{
    public class CGGPoint
    {
        private int x;
        private int y;

        public CGGPoint()
        {

        }

        public CGGPoint(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public int X
        {
            get { return x; }
            set { x = value; }
        }

        public int Y
        {
            get { return y; }
            set { y = value; }
        }
    }
}
