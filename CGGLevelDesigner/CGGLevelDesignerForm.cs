﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
//using Newtonsoft.Json;
using System.Xml;
using CGGClassLibrary;

namespace CGGLevelDesigner
{
    public partial class CGGLevelDesignerForm : Form
    {
        List<CGGPrimitive> rectList = new List<CGGPrimitive>();
        
        bool isClicked = false;
        
        CGGPrimitive selectedPrimitive = null;
        CGGPrimitive exchangeBuffer = null;

        int deltaX;
        int deltaY;
        
        double k;

        public CGGLevelDesignerForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            panel1.MouseDown += Form1_MouseDown;
            panel1.MouseMove += Form1_MouseMove;
            panel1.MouseUp += Form1_MouseUp;

            this.SizeChanged += Form1_SizeChanged;

            figureTypeToolStripComboBox.Items.Add("None");
            figureTypeToolStripComboBox.Items.Add("House");
            figureTypeToolStripComboBox.Items.Add("Pipe");

            figureTypeToolStripComboBox.SelectedIndexChanged
                += figureTypeToolStripComboBox_SelectedIndexChanged;

            scaleXToolStripTextBox.TextChanged += scaleXToolStripTextBox_TextChanged;
            scaleYToolStripTextBox.TextChanged += scaleYToolStripTextBox_TextChanged;
        }

        void scaleYToolStripTextBox_TextChanged(object sender, EventArgs e)
        {
            if (selectedPrimitive != null)
            {
                selectedPrimitive.ScaleY = Convert.ToDouble(scaleYToolStripTextBox.Text);
                ReDraw();
            }
        }

        void scaleXToolStripTextBox_TextChanged(object sender, EventArgs e)
        {
            if (selectedPrimitive != null)
            {
                selectedPrimitive.ScaleX = Convert.ToDouble(scaleXToolStripTextBox.Text);
                ReDraw();
            }
        }

        void figureTypeToolStripComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (selectedPrimitive != null)
            {
                selectedPrimitive.FigureType = (CGGFigureType)figureTypeToolStripComboBox.SelectedIndex;
            }
        }

        void Form1_SizeChanged(object sender, EventArgs e)
        {
            k = pictureBox1.ClientSize.Height / (double)pictureBox1.Image.Size.Height;
            double height = 800 * k;
            double width = 480 * k;

            toolStripStatusLabel2.Text = ((int)(height)).ToString() + "x" + ((int)(width)).ToString();

            panel1.Size = new System.Drawing.Size((int)width, (int)height);
            panel1.Location = new System.Drawing.Point(
                (int)((double)pictureBox1.ClientSize.Width / 2 - width / 2),
                (int)((double)pictureBox1.ClientSize.Height / 2 - height / 2 + pictureBox1.Location.Y));

            ReDraw();
        }

        void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            if (isClicked)
            {
                rectList.Add(selectedPrimitive);
                isClicked = false;
            }
        }

        void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (!isClicked)
            {
                return;
            }

            selectedPrimitive.X = (int)((e.X - deltaX) / k);
            selectedPrimitive.Y = (int)((e.Y + deltaY) / k);

            toolStripStatusLabel1.Text = e.X + "x" + e.Y;

            ReDraw();
        }

        void ReDraw()
        {
            var g = Graphics.FromHwnd(panel1.Handle);
            
            g.Clear(Color.White);
            
            var pen = new Pen(Color.Red);

            foreach (var figure in rectList)
            {
                g.DrawPolygon(pen, CGGPointListToPointArray(figure.GetPoints(k)));
            }

            if (selectedPrimitive != null)
            {
                g.DrawPolygon(new Pen(Color.Green),
                    CGGPointListToPointArray(selectedPrimitive.GetPoints(k)));
            }
        }

        void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            // Figure conteined in other figure does disappear
            var point = new CGGPoint(e.X, e.Y);
            for (var i = rectList.Count - 1; i >= 0; i--)
            {
                if (rectList[i].Contains(point, k))
                {
                    switch(e.Button)
                    {
                        case MouseButtons.Left:
                            isClicked = true;
                            deltaX = e.X - (int)(rectList[i].X * k);
                            deltaY = (int)(rectList[i].Y * k) - e.Y;
                            selectedPrimitive = rectList[i];
                            rectList.RemoveAt(i);
                            figureTypeToolStripComboBox.SelectedIndex = (int)selectedPrimitive.FigureType;
                            scaleXToolStripTextBox.Text = selectedPrimitive.ScaleX.ToString();
                            scaleYToolStripTextBox.Text = selectedPrimitive.ScaleY.ToString();
                            ReDraw();
                            break;
                        case MouseButtons.Right:
                            contextMenuStrip1.Show(panel1.Location.X + e.X, panel1.Location.Y + e.Y);
                            break;
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var g = Graphics.FromHwnd(panel1.Handle);
            
            var pen = new Pen(Color.Red);
            var rect = new CGGRect(0, 0);

            g.DrawPolygon(pen, CGGPointListToPointArray(rect.GetPoints(k)));

            rectList.Add(rect); 
        }

        private Point[] CGGPointListToPointArray(List<CGGPoint> cggPointList)
        {
            List<Point> points = new List<Point>();
            foreach (var point in cggPointList)
            {
                points.Add(new Point(point.X, point.Y));
            }

            return points.ToArray();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var g = Graphics.FromHwnd(panel1.Handle);
            rectList.Clear();
            g.Clear(SystemColors.Control);
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            exchangeBuffer = new CGGRect(selectedPrimitive.X, selectedPrimitive.Y);
            exchangeBuffer.ScaleX = selectedPrimitive.ScaleX;
            exchangeBuffer.ScaleY = selectedPrimitive.ScaleY;
            exchangeBuffer.FigureType = selectedPrimitive.FigureType;
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            exchangeBuffer.X = 0;
            exchangeBuffer.Y = 0;
            rectList.Add(exchangeBuffer);
            ReDraw();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "CGG level files (*.cgg)|*.cgg";

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                using (var fileStream = new FileStream(saveFileDialog.FileName, FileMode.Create))
                {
                    List<Type> knownTypeList = new List<Type>();
                    knownTypeList.Add(typeof(CGGRect));
                    //knownTypeList.Add(typeof(List<CGGPrimitive>));
                    //knownTypeList.Add(typeof(CGGPrimitive));
                    //knownTypeList.Add(typeof(List<CGGRect>));

                    DataContractSerializer dataContractSerizalizer =
                        new DataContractSerializer(rectList.GetType(), knownTypeList);
                    dataContractSerizalizer.WriteObject(fileStream, rectList);

                    //BinaryFormatter binaryFormatter = new BinaryFormatter();
                    //binaryFormatter.Serialize(fileStream, rectList);
                }
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "CGG level files (*.cgg)|*.cgg";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                using (var fileStream = new FileStream(openFileDialog.FileName, FileMode.Open))
                {
                    //XmlDictionaryReader xmlDictionaryReader = XmlDictionaryReader
                    //    .CreateTextReader(fileStream, new XmlDictionaryReaderQuotas());
                    List<Type> knownTypeList = new List<Type>();
                    knownTypeList.Add(typeof(CGGRect));

                    DataContractSerializer dataContractSerializer =
                        new DataContractSerializer(rectList.GetType(), knownTypeList);

                    rectList = dataContractSerializer.ReadObject(fileStream) as List<CGGPrimitive>;
                    //rectList = dataContractSerializer.ReadObject(xmlDictionaryReader, true)
                    //    as List<CGGPrimitive>;
                    //dataContractSerializer.ReadObject()
                    
                    //BinaryFormatter binaryFormatter = new BinaryFormatter();
                    //rectList = (List<CGGPrimitive>)binaryFormatter.Deserialize(fileStream);
                }
            }
            ReDraw();
        }
    }
}
